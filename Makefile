install:
	cp create-container.sh /usr/bin/devenv
	cp config /etc/devenv

uninstall:
	rm /usr/bin/devenv
	rm /etc/devenv

all: install

.PHONY: all
